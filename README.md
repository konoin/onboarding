Onboarding
============

This part of the application is written based on the lessons of Brian Wong, [link to youtube channel](https://www.youtube.com/channel/UCuP2vJ6kRutQBfRmdcI92mA).

## Notices
The code in this application is written using the UIKit framework, without the use of third-party libraries.

## Code
When working, the storyboard was not used. Everything is written using code.

![SceneDelegate](Image/Code.jpg)
![Code](Image/SceneDelegate.jpg)
![PageCell](Image/PageCell.jpg)

## Screenshots
Screenshots of the app itself.

![596](Image/Screen/IMG_0596.PNG)
![597](Image/Screen/IMG_0597.PNG)
![598](Image/Screen/IMG_0598.PNG)
![599](Image/Screen/IMG_0599.PNG)
![600](Image/Screen/IMG_0600.PNG)
![601](Image/Screen/IMG_0601.PNG)
![602](Image/Screen/IMG_0602.PNG)
![603](Image/Screen/IMG_0603.PNG)

